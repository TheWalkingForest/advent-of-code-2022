let rec read_file ic =
    match ic |> input_line with
    | line -> line :: (read_file ic)
    | exception _ -> []

let intersect l r =
    List.filter (fun e -> List.mem e r) l

let proccess d =
    let len = String.length d / 2 in
    let l = Str.first_chars d len |> String.to_seq |> List.of_seq in
    let r = Str.last_chars d len |> String.to_seq |> List.of_seq in
    intersect l r
        |> (fun x -> List.nth x 0)
        |> (fun x -> if Char.code x <= 90 then -38 + Char.code x else -96 + Char.code x)

let () = 
    let score = open_in "data.txt"
        |> read_file
        |> List.map ( proccess )
        |> List.fold_left (+) 0 in
    print_int score

