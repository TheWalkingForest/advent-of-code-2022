let rec read_file ic =
    match ic |> input_line with
    | line -> line :: (read_file ic)
    | exception _ -> []

let proccess_round a =
    match a with
        | "A X" -> 1 + 3
        | "A Y" -> 2 + 6
        | "A Z" -> 3 + 0
        | "B X" -> 1 + 0
        | "B Y" -> 2 + 3
        | "B Z" -> 3 + 6
        | "C X" -> 1 + 6
        | "C Y" -> 2 + 0
        | "C Z" -> 3 + 3
        | _ -> -1

let () =
    let score = open_in "data.txt"
        |> read_file
        |> List.map ( proccess_round )
        |> List.fold_left (+) 0 in
    print_int score

